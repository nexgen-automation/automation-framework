FROM nikolaik/python-nodejs:latest

LABEL Name="Automted Test Framwork" Version=1.4.2


COPY . /tmp
# RUN pip install --no-cache-dir -r requirements.txt
RUN apt update

# COPY . ./app

# EXPOSE 5000

WORKDIR /tmp
RUN chmod a+x /tmp/*
RUN chmod a+x ./test-cli.sh
 

CMD ["./test-cli.sh runtime"]

# CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]

